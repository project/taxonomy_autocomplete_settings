<?php
 

/**
 * @file taxonomy_autocomplete_settings.admin.inc
 * The taxonomy_autocomplete_settings admin file contains admin settings screens
 */

/**
 * settings form
 */
function taxonomy_autocomplete_settings_settings_form() {
  $result = db_query("SELECT vid, name FROM {vocabulary}");
  $list = array('match anything', 'match the beginning');
  
  while ($row = db_fetch_object($result)) {    
    $form['taxonomy_autocomplete_settings_'.$row->vid] = array(
      '#type' => 'select',
      '#options' => $list,
      '#title' => t('Autocomplete settings for vocabulary ' . $row->name),
      '#default_value' => variable_get('taxonomy_autocomplete_settings_'.$row->vid, array()),
    ); 
  }
  
  return system_settings_form($form); 
}